{
 "cells": [
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "# Part 3: Phase reconstruction\n",
    "Making use of the partial coherence of the X-ray beams at the synchrotron providing the edge enhancement, one can use the so-called single distance propagation-based phase contrast to boost the image contrast (this topic will be discussed in an upcoming lecture). For the purpose of this tutorial, we use a 1-dimensional phase filter on our 1-dimensional detector images to calculate the phase-filtered projection images (fltp - **f**i**lt**ered **p**rojections). The filtering is based on some physical parameters (energy, distance, etc.), which have to be specified for the particular data acquisition conditions of the particular sample.\n",
    "\n",
    "The method used here is based on the so-called Paganin phase reconstruction as proposed by D. Paganin et al. (http://doi.wiley.com/10.1046/j.1365-2818.2002.01010.x)."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "scrolled": true
   },
   "outputs": [],
   "source": [
    "# Load the necessary packages and prepare the python environment\n",
    "import matplotlib.pyplot as plt\n",
    "import numpy as np\n",
    "import time\n",
    "from skimage.filters import threshold_otsu\n",
    "\n",
    "from tomography_tutorial_functions import *\n",
    "from paganin_functions import *\n",
    "\n",
    "%matplotlib inline"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Applying the phase contrast reconstruction to the low SNR dataset from before reveals a rather remarkable boost of the image contrast."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# Data acquisition parameters\n",
    "energy = 20.0\n",
    "pixel_size = 2.75e-6\n",
    "dist = 0.305\n",
    "\n",
    "# Sample properties\n",
    "delta = 1e-7\n",
    "beta = 2e-9\n",
    "\n",
    "dataset = 'Data/GF_spheres_dry_8000prj_305mm_lowSNR_01_slice310.h5'\n",
    "max_width = 600\n",
    "prj, dark, white, theta = load_data(dataset, max_width=max_width)\n",
    "prj, theta = reduce_projections(10, prj, theta)\n",
    "image_width = prj.shape[1]\n",
    "center = (image_width / 2.0) - 2\n",
    "padding = 0.45\n",
    "cpr = correct_data(prj, dark, white)\n",
    "\n",
    "# Calculate the standard absorption reconstruction\n",
    "sino = -np.log(cpr)\n",
    "reco = reconstruct(sino, theta, center=center, padding=padding, verbose=False)\n",
    "\n",
    "# Calculate the phase-filtered projections\n",
    "fltp = paganin_1Dfilter(cpr, energy, pixel_size, delta, beta, dist, prepad=True)\n",
    "\n",
    "# The above phase filter step already includes taking the negative logarithm of\n",
    "# the measured transmitted intensities, so the sinogram is identical to the\n",
    "# filtered projections.\n",
    "sino_phase = fltp\n",
    "\n",
    "# Run the reconstruction of the phase-filtered sinograms\n",
    "reco_phase = reconstruct(sino_phase, theta, center=center, padding=0.7, filter='ramp')"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# Plot the phase-reconstruction compared to the standard absorption reconstruction\n",
    "plt.figure(figsize=[16,8])\n",
    "plt.subplot(1,2,1)\n",
    "plt.imshow(reco, cmap='gray')\n",
    "plt.title(\"Absorption reconstruction\")\n",
    "plt.subplot(1,2,2)\n",
    "plt.imshow(reco_phase, cmap='gray')\n",
    "plt.title(\"Phase reconstruction\")\n",
    "plt.imshow(reco_phase, cmap='gray')"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "The boost in image contrast in the phase reconstruction can be used to somewhat counter-act the loss of image quality when speeding up the tomography scans. This is used often when pushing the time-resolution towards faster and faster scan times for rapidly changing samples in a dynamic state.\n",
    "\n",
    "Below, we compare the reconstruction results for the original exposure time and for a 10x reduced exposure time per projection."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# Run the absorption and phase reconstructions on the above dataset with a 2x lower exposure time\n",
    "\n",
    "# Increase the delta/beta ratio a bit to get more phase contrast\n",
    "delta_e10 = 1e-7\n",
    "beta_e10 = 5e-10\n",
    "\n",
    "prj_e10, dark_e10, white_e10 = reduce_exposure_time(0.5, prj, dark, white)\n",
    "cpr_e10 = correct_data(prj_e10, dark_e10, white_e10)\n",
    "reco_e10 = reconstruct(-np.log(cpr_e10), theta, center=center, padding=padding, verbose=False)\n",
    "\n",
    "fltp_e10 = paganin_1Dfilter(cpr_e10, energy, pixel_size, delta_e10, beta_e10, dist, prepad=True)\n",
    "reco_phase_e10 = reconstruct(fltp_e10, theta, center=center, padding=0.7, filter='ramp')"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# Compare the results visually\n",
    "plt.figure(figsize=[16,16])\n",
    "plt.subplot(2,2,1)\n",
    "plt.imshow(reco, cmap='gray')\n",
    "plt.title(\"abs. reco: original exp.\")\n",
    "plt.subplot(2,2,2)\n",
    "plt.imshow(reco_phase, cmap='gray')\n",
    "plt.title(\"phase reco: original exp.\")\n",
    "plt.subplot(2,2,3)\n",
    "plt.imshow(reco_e10, cmap='gray')\n",
    "plt.title(\"abs. reco: exp. reduced 10x\")\n",
    "plt.subplot(2,2,4)\n",
    "plt.imshow(reco_phase_e10, cmap='gray')\n",
    "plt.title(\"phase reco: exp. reduced 10x\")"
   ]
  }
 ],
 "metadata": {
  "kernelspec": {
   "display_name": "Python 3",
   "language": "python",
   "name": "python3"
  },
  "language_info": {
   "codemirror_mode": {
    "name": "ipython",
    "version": 3
   },
   "file_extension": ".py",
   "mimetype": "text/x-python",
   "name": "python",
   "nbconvert_exporter": "python",
   "pygments_lexer": "ipython3",
   "version": "3.8.5"
  }
 },
 "nbformat": 4,
 "nbformat_minor": 4
}
