{
 "cells": [
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "# Part 6: Tomoscopy - Time-resolved tomography\n",
    "*Tomoscopy* is a synonym for time-resolved tomography in dynamic systems, meaning systems that undergo temporal changes in their structure.\n",
    "\n",
    "The main challenge in time-resolved tomography is to match the time it takes to acquire a tomographic scan to the typical time scales in the system. The scan needs to be fast enough such that the structural changes within the sample are so small that it appears static during the scan. If this condition is not met, the scans are exhibiting motion artefacts that will render the data useless or at least obscure the dymanic events of interest.\n",
    "\n",
    "Just as in the dose optimization challenge, the goal is to keep the highest possible image quality at the necessary time resolution.\n",
    "\n",
    "## Data sets\n",
    "We have two data sets from time-resolved experiments available. You can use the `load_scan_series()` function to load these data sets with the dataset name provided in the table.\n",
    "\n",
    "| Dataset name | Energy [keV] | Pixel size [um] | Exposure time [ms]| Distance [m] | # of pixels | rotation center | # of projections | # of projections / rotation | shortest scan time [ms] | Description |\n",
    "| :--- | :---: | :---: | :---: | :---: | :---: | :--- | :--- | :--- | :--- | :--- |\n",
    "| 'metal_foam' | peak @ 24 | 2.75 | 0.025 | 0.250 | 528 | ca. 263 | 3000 | ca. 40 | 1 | Aluminum metal foam |\n",
    "| 'coffee' | peak @ 24 | 2.75 | 1.25 | 0.305 | 500 | ca. 245 | 48000 | ca. 40 | 50 | Nespresso coffee foam |\n",
    "\n",
    "More information on the individual data sets will be provided below."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "scrolled": true
   },
   "outputs": [],
   "source": [
    "# Load the necessary packages and prepare the python environment\n",
    "import matplotlib.pyplot as plt\n",
    "import numpy as np\n",
    "import time\n",
    "from skimage.filters import threshold_otsu\n",
    "\n",
    "from tomography_tutorial_functions import *\n",
    "from paganin_functions import *\n",
    "\n",
    "%matplotlib inline"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Bubble coalescence in a metal foam\n",
    "Certain metal alloys can form foams when heating them up and melting the ingredients. These materials are very lightweight and structurally solid, making them perfect for engineering.\n",
    "\n",
    "The formation of the bubbles and their evolution take place on the second to millisecond time scale. The rupture event of two bubbles merging into a single one lasts of the order of 1 ms or even less. The shape equilibration of the resulting larger bubble takes a few to a few tens of milliseconds.\n",
    "\n",
    "For this tutorial, we were granted access to the worlds fastest recorded tomography data set with micron resolution (many thanks to the group of F. Garcia-Moreno from the Helmholtz Zentrum Berlin, who have measured this dataset at the TOMCAT beamline). Details about the used setup and the achieved results can be found in this paper:\n",
    "* https://onlinelibrary.wiley.com/doi/10.1002/adma.202104659 \n",
    "\n",
    "We use here a sub-set of the complete data set that comprises a total of 75 ms of measurements. The fastest possible time-resolution is 1 ms. To simulate a choice of scan time, the `load_scan_series()` function used below will integrate the original scan data over the specified number of scans. So, for example, we can either obtain 75 scans of 1 ms, or 25 scans of 3 ms each.\n",
    "\n",
    "You will note that the image quality is much poorer than for the above examples, but keep in mind that the scan was also aquired nearly 500'000 times faster! There are only 40 projections over 180 degrees, so you will see the familiar artefacts we observed in Part 2.\n",
    "\n",
    "### Acquisition parameters:\n",
    "* Energy: 24 keV\n",
    "* Distance: 250 mm\n",
    "* Pixel size: 2.75 um\n",
    "* The center is approximately at 263\n",
    "\n",
    "### Exercise:\n",
    "Explore the tomoscopy data set with the functions you mastered in the sections above. The cells below will provide you with some help to get started. Try to play with simulating different scan times and how they affect the quality of the final reconstruction. There is indeed one very fast event hidden in the data set. Can you find it? Can you determine approximately at what time after the start of the time series the event happens?"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# Define some starting parameters\n",
    "center = 263\n",
    "padding = 0.5\n",
    "energy = 24.0\n",
    "pixel_size = 2.75e-6\n",
    "dist = 0.25"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "scrolled": true
   },
   "outputs": [],
   "source": [
    "# Load the time-series data from file\n",
    "\n",
    "# Define the simulated scan time.\n",
    "# This means that in the loaded data set, `scan_time` scans will be averaged together to one new data set.\n",
    "# By doing so, the image quality might increase, but the time-resolution is, of course, decreased\n",
    "# by a factor of `scan_time` with respect to the original data set.\n",
    "scan_time = 6 \n",
    "\n",
    "scan_data, dark, white, scan_theta = load_scan_series('metal_foam', scan_time=scan_time)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# Note how scan_data and scan_theta are lists with one entry for every scan in the simulated time series:\n",
    "print(len(scan_data))\n",
    "print(scan_data[0].shape)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# Select the projections and theta values for one time step\n",
    "scan_number = 2\n",
    "prj = scan_data[scan_number]\n",
    "theta = scan_theta[scan_number]"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# Run a reconstruction on one of the simulated time points\n",
    "cpr = correct_data(prj, dark, white)\n",
    "reco_abs = reconstruct(-np.log(cpr), theta, center=center, padding=padding, verbose=False)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# Plot the reconstructed image\n",
    "plt.figure(figsize=[6,6])\n",
    "plt.imshow(reco_abs, cmap='gray')"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# Try a phase reconstruction\n",
    "delta = 2e-7\n",
    "beta = 1e-9\n",
    "fltp = paganin_1Dfilter(cpr, energy, pixel_size, delta, beta, dist, prepad=True)\n",
    "reco_phase = reconstruct(fltp, theta, center=center, padding=0.7, filter='ramp', verbose=False)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# Plot the phase reconstruction\n",
    "plt.figure(figsize=[6,6])\n",
    "plt.imshow(reco_phase, cmap='gray')"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# display a time-series \"movie\" \n",
    "\n",
    "# the next two lines are silly, but they make sure that jupyter is not scrolling in a funny way...\n",
    "fig = plt.figure(figsize=[8,8])\n",
    "plt.close(fig)\n",
    "\n",
    "# prepare a new figure\n",
    "fig = plt.figure(figsize=[8,8])\n",
    "fig_handle = display(fig, display_id=True) # this is some jupyter magic required to update plots live...\n",
    "ax = plt.gca()\n",
    "\n",
    "for i in range(len(scan_data)):\n",
    "    cpr = correct_data(scan_data[i], dark, white)\n",
    "    reco_abs = reconstruct(-np.log(cpr), scan_theta[i], center=center, padding=0, verbose=False)\n",
    "    ax.cla() # clear the axis of the plot\n",
    "    ax.imshow(reco_abs, cmap='gray')\n",
    "    ax.set_title(f\"Time step {i}\")\n",
    "    fig.canvas.draw() # this updates the plot\n",
    "    fig_handle.update(fig) # this updates the figure\n",
    "\n",
    "plt.close(fig)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# Your code here..."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Coffee foam\n",
    "\n",
    "The second data set (`'coffee'`) is the foam taken from a freshly extracted Nespresso coffee. It was placed in the beamline within 30 seconds of being produced and then scanned with 20 scans per second (of about 40 projections each).\n",
    "\n",
    "### Exercise:\n",
    "\n",
    "Use all the knowledge you have gained throughout this tutorial to address a few of the following questions:\n",
    "* Can you find an optimal time resolution for the scanning to avoid motion blurring and maximize the image quality?\n",
    "* Can you count the number of bubbles present in the foam as a function of time?\n",
    "* How does the average bubble size and the bubble size distribution change over time?\n"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# Define some starting parameters\n",
    "center = 245\n",
    "padding = 0.45\n",
    "energy = 24.0\n",
    "pixel_size = 2.75e-6\n",
    "dist = 0.305"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# Load the time-series data from file\n",
    "\n",
    "# Define the simulated scan time.\n",
    "# This means that in the loaded data set, `scan_time` scans will be averaged together to one new data set.\n",
    "# By doing so, the image quality might increase, but the time-resolution is, of course, decreased\n",
    "# by a factor of `scan_time` with respect to the original data set.\n",
    "scan_time = 4 # your value here... \n",
    "\n",
    "scan_data, dark, white, scan_theta = load_scan_series('coffee', scan_time=scan_time)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# Your code here..."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": []
  }
 ],
 "metadata": {
  "kernelspec": {
   "display_name": "Python 3",
   "language": "python",
   "name": "python3"
  },
  "language_info": {
   "codemirror_mode": {
    "name": "ipython",
    "version": 3
   },
   "file_extension": ".py",
   "mimetype": "text/x-python",
   "name": "python",
   "nbconvert_exporter": "python",
   "pygments_lexer": "ipython3",
   "version": "3.8.5"
  }
 },
 "nbformat": 4,
 "nbformat_minor": 4
}
