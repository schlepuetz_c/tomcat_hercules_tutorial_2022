{
 "cells": [
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "# Part 4: Basic image analysis\n",
    "\n",
    "The reconstructed images are usually further analyzed to extract quantitative information from the volume data sets (in our case, we just have one slice). Examples of such quantifications are:\n",
    "\n",
    "* packing density of granular materials\n",
    "* pore space volume in a porous medium\n",
    "* particle size distributions\n",
    "* etc."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "scrolled": true
   },
   "outputs": [],
   "source": [
    "# Load the necessary packages and prepare the python environment\n",
    "import matplotlib.pyplot as plt\n",
    "import numpy as np\n",
    "import skimage.morphology as morph\n",
    "import skimage.measure as measure\n",
    "from scipy import ndimage\n",
    "import skimage.segmentation as segmentation\n",
    "import time\n",
    "from skimage.filters import threshold_otsu\n",
    "\n",
    "from tomography_tutorial_functions import *\n",
    "from paganin_functions import *\n",
    "\n",
    "%matplotlib inline"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## 1- Image thresholding / segmentation\n",
    "A first step in image analysis is very often the distinction between different phases (e.g. air vs. material) in the image. The simplest approach to perform this so-called segmentation is via thresholding the grayscale image to produce a binary image where the zeros correspond to one phase and the ones to the other phase."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### a) Manual image thresholding\n",
    "\n",
    "Let's take our previous dataset with PMMA spheres and try a quick and manual thresholding operation on it.\n",
    "\n",
    "The first step is to display the image histogram (visualization of the grey level distribution), and adjust the threshold according to what we want to segment (here, the individual spheres).\n",
    "The threshold is then used to create the binary image (pixel value =1 when above the threshold, =0 when below)."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# Data acquisition parameters\n",
    "energy = 20.0\n",
    "pixel_size = 2.75e-6\n",
    "dist = 0.305\n",
    "\n",
    "# Sample properties\n",
    "delta = 1e-7\n",
    "beta = 2e-9\n",
    "\n",
    "# Load and correct the sphere datasets (high SNR, 400 projections) \n",
    "max_width = 620\n",
    "dataset = 'Data/GF_spheres_dry_8000prj_305mm_highSNR_01_slice310.h5'\n",
    "prj0, dark, white, theta0 = load_data(dataset, max_width=max_width)\n",
    "# reduce the number of projections used to speed up the calcualtions\n",
    "prj, theta = reduce_projections(20, prj0, theta0)\n",
    "\n",
    "image_width = prj.shape[1]\n",
    "center = (image_width / 2.0) - 2\n",
    "padding = 0.45\n",
    "cpr = correct_data(prj, dark, white)\n",
    "\n",
    "# Reconstruction in absorption\n",
    "sino = -np.log(cpr)\n",
    "reco = reconstruct(sino, theta, center=center, padding=padding, verbose=False)\n",
    "\n",
    "# Reconstruction in phase\n",
    "sino_phase = paganin_1Dfilter(cpr, energy, pixel_size, delta, beta, dist, prepad=True)\n",
    "reco_phase = reconstruct(sino_phase, theta, center=center, padding=0.7, filter='ramp')\n",
    "\n",
    "# Plot\n",
    "plt.figure(figsize=[16,8])\n",
    "plt.subplot(1,2,1)\n",
    "plt.imshow(reco, cmap='gray')\n",
    "plt.title(\"Absorption reconstruction\")\n",
    "plt.subplot(1,2,2)\n",
    "plt.imshow(reco_phase, cmap='gray')\n",
    "plt.title(\"Phase reconstruction\")\n",
    "plt.imshow(reco_phase, cmap='gray')"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# Histogram of the image\n",
    "plt.figure(figsize=[16,4])\n",
    "\n",
    "plt.subplot(1,2,1)\n",
    "hist,axis = np.histogram(reco,bins=128)\n",
    "plt.plot(axis[0:-1],hist)\n",
    "plt.xlabel('Gray value')\n",
    "plt.ylabel('Counts')\n",
    "plt.title(\"Absorption histogram\")\n",
    "\n",
    "plt.subplot(1,2,2)\n",
    "hist,axis = np.histogram(reco_phase,bins=128)\n",
    "plt.plot(axis[0:-1],hist)\n",
    "plt.xlabel('Gray value')\n",
    "plt.ylabel('Counts')\n",
    "plt.title(\"Phase histogram\")"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "What conclusions can you draw from these 2 histograms? Which contrast would you better use to further proceed with segmentation of the individual spheres?"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# Select the optimum reconstrcution\n",
    "img = reco_phase\n",
    "#img = reco\n",
    "\n",
    "# Adjust the threshold\n",
    "th=0.1e-7\n",
    "\n",
    "# Create the binary image\n",
    "bimg=th<img\n",
    "plt.figure(figsize=[9,9])\n",
    "plt.imshow(bimg)\n",
    "plt.title('th='+str(th))"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### b) Automatic segmentation\n",
    "\n",
    "Different methods exist to automatically optimize the threshold. Let's try it now!\n",
    "\n",
    "For a quick visual explanation of the automatic thresholding, and the function we will be using for it, see, for example, here:\n",
    "https://scikit-image.org/docs/dev/auto_examples/segmentation/plot_thresholding.html"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# Calculate the threshold values using the Otsu method\n",
    "auto_thresh_abs = threshold_otsu(reco)\n",
    "auto_thresh_phase = threshold_otsu(reco_phase)\n",
    "\n",
    "# Plot the resulting threshold values in the corresponding histograms\n",
    "plt.figure(figsize=[16,4])\n",
    "plt.subplot(1,2,1)\n",
    "h = plt.hist(np.ravel(reco), 256)\n",
    "plt.axvline(auto_thresh_abs, color='r')\n",
    "plt.title(\"Absorption reconstruction\")\n",
    "plt.subplot(1,2,2)\n",
    "h = plt.hist(np.ravel(reco_phase), 256)\n",
    "plt.axvline(auto_thresh_phase, color='r')\n",
    "plt.title(\"Phase reconstruction\")"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# Calculate the segmented images\n",
    "segmented_abs = reco > auto_thresh_abs\n",
    "segmented_phase = reco_phase > auto_thresh_phase\n",
    "\n",
    "# Plot the results\n",
    "plt.figure(figsize=[16,16])\n",
    "plt.subplot(2,2,1)\n",
    "plt.imshow(reco, cmap='gray')\n",
    "plt.title(\"Absorption reconstruction\")\n",
    "plt.subplot(2,2,2)\n",
    "plt.imshow(reco_phase, cmap='gray')\n",
    "plt.title(\"Phase reconstruction\")\n",
    "plt.subplot(2,2,3)\n",
    "plt.imshow(segmented_abs, cmap='gray')\n",
    "plt.title(\"Abs. reco: automatic segmentation\")\n",
    "plt.subplot(2,2,4)\n",
    "plt.imshow(segmented_phase, cmap='gray')\n",
    "plt.title(\"Phase reco: automatic segmentation\")"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## 2- Simple statistics on the image\n",
    "\n",
    "Now we would like to obtain quantitative information on our image, like the number of spheres or their mean area.\n",
    "A way to do it is to label each individual feature in our freshly created bilevel image. This can be done with the _label_ methods from the _measure_ library.\n",
    "\n",
    "However, the segmentation done before is not perfect and some pixels are misclassified. \n",
    "Our bilevel image needs to be cleaned by some morphological operation before labelling.\n",
    "\n",
    "Doc about _morphology_ module: https://scikit-image.org/docs/dev/api/skimage.morphology.html\n",
    "\n",
    "Doc about _measure_ module: https://scikit-image.org/docs/dev/api/skimage.measure.html"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# Choose your favorite segmentation\n",
    "bimg = segmented_phase\n",
    "#bimg = segmented_abs\n",
    "\n",
    "# Apply cleaning operation (opening = an erosion followed by a dilation)\n",
    "#clean0=morph.erosion(bimg,morph.square(4))\n",
    "#clean=morph.dilation(clean0,morph.square(4))\n",
    "clean=morph.opening(bimg,morph.square(6))\n",
    "\n",
    "# Label individual shperes\n",
    "lbl, num = measure.label(clean,return_num=True)\n",
    "\n",
    "# Plot\n",
    "plt.figure(figsize=[16,16])\n",
    "plt.subplot(2,2,1)\n",
    "plt.imshow(reco_phase, cmap='gray')\n",
    "plt.title(\"Phase reconstruction\")\n",
    "plt.subplot(2,2,2)\n",
    "plt.imshow(bimg)\n",
    "plt.title('Bilevel original image')\n",
    "plt.subplot(2,2,3)\n",
    "plt.imshow(clean)\n",
    "plt.title('Clean image')\n",
    "plt.subplot(2,2,4)\n",
    "plt.imshow(lbl,cmap=plt.get_cmap('tab20'))\n",
    "plt.title('Labeled image')"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "We can see that segmentation and feature labelling are not easy things to automatize!\n",
    "\n",
    "\n",
    "To have a rough approximation of the number of spheres in the slice, we can use the number of features labelled by the _measure.label_ method.  \n",
    "Other properties can be extracted from each label: area, perimeter, minor and major axis length, etc.\n",
    "\n",
    "Have a look at this doc to see the exhaustive list of properties, and play with it:   \n",
    "https://scikit-image.org/docs/dev/api/skimage.measure.html#skimage.measure.regionprops\n",
    "\n",
    "What is the mean area of the spheres?"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "print('Number of spheres in the slice (rough approximation) = '+str(num))\n",
    "\n",
    "m=measure.regionprops(lbl)\n",
    "\n",
    "# Property of spheres: minor axis length = major axis length\n",
    "# Let's check if our features verify this property!\n",
    "p1=[]\n",
    "p2=[]\n",
    "for prop in m:\n",
    "    p1.append(prop.minor_axis_length)\n",
    "    p2.append(prop.major_axis_length)\n",
    "\n",
    "plt.plot(p1,p2,'+')\n",
    "plt.xlabel('minor axis')\n",
    "plt.ylabel('major axis')\n"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Bonus...\n",
    "Another way to perform a segmentation and to label the features is to use the watershed segmentation.\n",
    "\n",
    "To segment a binary image with the watershed approach, you have to compute its distance map.  \n",
    "You also need a marker image that tells the algorithm where to start.\n",
    "\n",
    "\n",
    "Doc: https://scikit-image.org/docs/dev/api/skimage.segmentation#skimage.segmentation.watershed"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# Watershed segmentation\n",
    "img = clean\n",
    "distance = ndimage.distance_transform_edt(img)\n",
    "h=2 # what happens when h is changed?\n",
    "plt.figure(figsize=[16,16])\n",
    "plt.subplot(2,2,1)\n",
    "plt.imshow(img)\n",
    "plt.title('Bilevel input image')\n",
    "plt.subplot(2,2,2)\n",
    "plt.imshow(distance)\n",
    "plt.title('Bilevel input image')\n",
    "plt.subplot(2,2,3)\n",
    "local_maxi=morph.h_maxima(distance,h)\n",
    "markers = measure.label(local_maxi)\n",
    "plt.imshow(markers,vmin=0,vmax=10)\n",
    "plt.title('Markers')\n",
    "plt.subplot(2,2,4)\n",
    "labels_ws = segmentation.watershed(-distance, markers, mask=img)\n",
    "plt.imshow(labels_ws,cmap=plt.get_cmap('tab20'))\n",
    "plt.title('Watershed segmented grains')"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": []
  }
 ],
 "metadata": {
  "kernelspec": {
   "display_name": "Python 3",
   "language": "python",
   "name": "python3"
  },
  "language_info": {
   "codemirror_mode": {
    "name": "ipython",
    "version": 3
   },
   "file_extension": ".py",
   "mimetype": "text/x-python",
   "name": "python",
   "nbconvert_exporter": "python",
   "pygments_lexer": "ipython3",
   "version": "3.8.10"
  }
 },
 "nbformat": 4,
 "nbformat_minor": 4
}
