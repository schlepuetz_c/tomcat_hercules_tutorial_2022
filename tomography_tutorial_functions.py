#!/usr/bin/env python
# coding: utf-8

__author__ = 'Christian M. Schlepuetz, Federica Marone'
__date_created__ = '2021-03-07'
__credits__ = ''
__copyright__ = 'Copyright (c) 2021-2022, Paul Scherrer Institut'
__docformat__ = 'restructuredtext en'


import h5py
import numpy as np
import os
import time

from skimage.transform import iradon, downscale_local_mean
from skimage.transform import AffineTransform, warp


def adjust_rotation_center(sino, center):
    """
    Adjust the rotation center by shifting (and edge-padding) the sinograms.

    Parameters
    ----------
    sino : array-like
        The sinogram to be adjusted
    center : float
        The pixel position which is to be centered in the sinogram.

    Returns
    -------
    shifted_sino : array-like
        The shifted sinogram.

    """

    w = sino.shape[1]
    vector = [center - w/2.0, 0]
    transform = AffineTransform(translation=vector)
    shifted_sino = warp(sino, transform, mode='edge', preserve_range=True)

    shifted_sino = shifted_sino.astype(sino.dtype)
    return shifted_sino

def correct_data(prj, dark, white):
    """
    Apply dark and flat field correction to projection data.

    Parameters
    ----------
    prj : array-like
        The projection data
    dark: 2D array
        The dark field data set.
    white : 2D array
        The white field data set.

    Returns
    -------
    cpr : array-like
        The corrected projection data

    """

    dark = np.median(dark, axis=0)
    white = np.median(white, axis=0)
    # avoid divisions by zero if white counts are very low...
    denominator = np.maximum((white - dark), np.ones(white.shape))
    cpr = (prj - dark[None,:]) / denominator[None,:]
    if np.min(np.ravel(cpr)) <= 0:
        print('WARNING: There are pixels with zero or negative intensity '
              'values in the corrected projections! Resetting those values to'
              '1e-7 to avoid division by zero or log or zero errors.')
        cpr[cpr <= 0] = 1e-7
    return cpr

def load_data(filename='149_ASM_SP_1ktps_subset_withTheta_slice50.h5',
              slice_num=0, max_width=0, binning=1, verbose=True):
    """
    Load a single slice (detector row) of data from a data exchange file.

    Data is returned as arrays with the first dimension corresponding to the
    angular scan dimension (number of images) and the second dimension the
    number of horizontal pixels in the line images.

    Parameters
    ----------
    filename : str
        The file name (including path) of the file to load data from.
    slice_num : int, optional
        The slice number (pixel row number) to be extracted from the original
        data set. (default = 0 --> works for data sets that only have one
        slice already).
    max_width : int, optional
        The maximum width of the image data to return. If the actual image data
        is wider than `max_width`, it will be cropped symmetrically around the
        image center to `max_width`. If set to 0, no cropping will be applied.
        (default = 0)
    binning : int, optional
        Binning factor for the projection data (default=1)
    verbose : bool, optional
        Verbosity of the function. Displays information if set to True.
        (default = True)

    Returns
    -------
    data : 2D array
        The projection data set.
    data_dark: 2D array
        The dark field data set.
    data_white : 2D array
        The white field data set.
    theta : 1-D array
        The measured angles for the projection data. If no angular information
        is present in the file, values for the interval [0, 180) are assumed
        and calculated based on the number of images.

    """

    with h5py.File(filename, 'r') as fh:
        data = np.array(fh['/exchange/data'][:,slice_num,:])
        data_dark = np.array(fh['/exchange/data_dark'][:,slice_num,:])
        data_white = np.array(fh['/exchange/data_white'][:,slice_num,:])
        if '/exchange/theta' in fh:
            theta = np.array(fh['/exchange/theta'][:])
        else:
            theta = np.linspace(0, 180.0, data.shape[0], endpoint=False)

        if binning > 1:
            # bin only along the image dimension, not the angular coordinate
            data = downscale_local_mean(data, (1,binning))
            data_dark = downscale_local_mean(data_dark, (1,binning))
            data_white = downscale_local_mean(data_white, (1,binning))

        data_width = data.shape[1]
        if (max_width > 0) and (data_width > max_width):
            left_crop = int(np.floor((data_width - max_width) / 2.0))
            data = data[:, left_crop:(left_crop + max_width)]
            data_dark = data_dark[:, left_crop:(left_crop + max_width)]
            data_white = data_white[:, left_crop:(left_crop + max_width)]

        if verbose:
            print("Loaded data set: {}".format(filename))
            print("  Number of projections: {}".format(data.shape[0]))
            print("  Number of darks: {}".format(data_dark.shape[0]))
            print("  Number of whites: {}".format(data_white.shape[0]))
            print("  Maximum image width: {}".format(max_width))
            print("  Binning factor: {}".format(binning))
            print("  Width of binned data: {} pixels".format(data_width))
            print("  Width of cropped data: {} pixels".format(data.shape[1]))

        return data, data_dark, data_white, theta

def make_slice_file(filename, slice_num, h_crop=None):
    """
    Create a data exchange file with just one pixel row from a full data file.

    This function just extracts only the data for one single slice from a full
    volume data set (mimicking a horizontal line detector) and saves it into
    a new file. The structure of the /exchange/data* datasets remains the same,
    so all of the usual tools to process the data set should still work. The
    dimensions of the data set will just be [n_angles, 1, h_width], where
    n_angles is the number of acquired projection angles and h_width is the
    horizontal with of the detector. Optionally, the horizontal width can also
    be cropped down with the h_crop parameter.

    The output data file will be stored in the same location as the original
    file with an appended suffix of the form "_slice<N>", where <N> is the
    slice number.

    Parameters
    ----------
    filename : str
        The file name (including path) of the original data file
    slice_num : int
        The slice number (pixel row number) to be extracted from the original
        data set.
    h_crop : int, array-like, optional
        2-element array with the lower and upper column indices to apply the
        cropping. If set to None, no cropping will be applied. (Default = None)

    """

    fname, ext = os.path.splitext(os.path.abspath(os.path.expanduser(filename)))
    foutname = fname + '_slice{}'.format(slice_num) + ext
    with h5py.File(filename, 'r') as fho:
        width = fho['/exchange/data'].shape[2]
        if h_crop is None:
            x1 = 0
            x2 = width
        else:
            x1 = h_crop[0]
            x2 = h_crop[1]
        data = fho['/exchange/data'][:,slice_num,x1:x2]
        data_dark = fho['/exchange/data_dark'][:,slice_num,x1:x2]
        data_white = fho['/exchange/data_white'][:,slice_num,x1:x2]
        with h5py.File(foutname, 'w') as fhn:
            fhn.create_dataset('implements', data='exchange')
            ge = fhn.require_group('exchange')
            ge.create_dataset('data', data=np.expand_dims(data, 1))
            ge.create_dataset('data_white', data=np.expand_dims(data_white, 1))
            ge.create_dataset('data_dark', data=np.expand_dims(data_dark, 1))
            if '/exchange/theta' in fho:
                theta = fho['/exchange/theta'][:]
                ge.create_dataset('theta', data=theta)

def reconstruct(sino, theta, center=None, padding=0, filter='hamming',
                verbose=True):
    """
    Calculate the reconstruction (inverse Radon transform) from sinogram and
    angle data.

    Parameters
    ----------
    sino : float, 2D array-like
        The sinogram to be reconstructed. The first dimension is the number of
        projections, the second dimension the number of pixels.
    theta : float, 1D array-like
        The array of angular positions for the projections (in degrees). Must
        have the same length as the first sinogram dimension.
    center : None or float
        The center position for the reconstruction. If None, the middle of the
        detector will be used. (default = None)
    padding : float
        The amount of constant padding to be applied to the sinogram before
        reconstruction. The padding amount is specified as a fraction of the
        projection width (padding = 1 would double the sinogram size).
        (default = 0)
    filter : str
        The filter to use in the reconstruction. Valid filters are those
        allowed by the :func:`skimage.transform.iradon` function:
        {None, 'ramp', 'shepp-logan', 'cosine', 'hamming', 'hann'}
        (default = 'hamming')
    verbose : bool, optional
        Verbosity of the function. Displays information if set to True.
        (default = True)

    Returns
    -------
    reco : 2D array
        The reconstructed slice image (cropped back to the original detector
        size before padding).

    """

    _t1 = time.time()
    w = sino.shape[1]
    w_p = 0
    if padding > 0:
        w_p = int((padding * w) / 2.0)
        sino = np.pad(sino, ((0, 0), (w_p, w_p)), mode='edge')

    if center is not None:
        cen = center + w_p
        sino = adjust_rotation_center(sino, cen)
        cen_str = "{}".format(center)
    else:
        cen = (w / 2.0) + w_p
        center = (w / 2.0)
        cen_str = "auto ({})".format(center)

    if verbose:
        print("Running reconstruction:")
        print("  Number of projections: {}".format(sino.shape[0]))
        print("  Number of original pixels: {}".format(w))
        print("  Center: {}".format(cen_str))
        print("  Padding: {} pixels on each side".format(w_p))
        print("  Number of pixels with padding: {}".format(sino.shape[1]))

    reco = iradon(np.transpose(sino), theta=theta, filter_name=filter)

    if padding > 0:
        reco = reco[w_p:-w_p, w_p:-w_p]

    if verbose:
        print("Reconstruction took {:.1f} seconds to compute".format(
            time.time() - _t1))

    return reco

def reconstruct_absorption(prj, dark, white, theta, center=None, padding=0,
                           filter='hamming', verbose=True):
    """

    """

    cpr = correct_data(prj, dark, white)
    sino = -np.log(cpr)
    reco = reconstruct(sino, theta, center=center, padding=padding,
        filter=filter, verbose=verbose)
    return reco

def reduce_exposure_time(factor, prj, dark, white):
    """
    Simulate a reduction of exposure time in the data set.

    Parameters
    ----------
    factor : float
        The factor to scale the exposure time. Should be a number between 0
        and 1.
    prj : array-like
        The original projection data to be scaled.
    dark : array-like
        The original dark field data
    white : array-like
        The original white field data

    Returns
    -------
    scaled_prj : array-like
        The simulated projection data for the reduced exposure time
    dark : array-like
        A copy of the original dark field data (not scaled)
    scaled_white : array-like
        The simulated white field data for the reduced exposure time

    """

    # * assume that the original input data has no significant noise
    #   (noise level negligible due to very high signal to noise ratio)
    # * reduce the count numbers by the given factor, and add random noise to
    #   the result with a poisson distribution around the reduced intensity
    #   value.
    # * assume that the darks are independent of the exposure time (not sure
    #   how true this is...)

    if factor <= 0 or factor > 1:
        raise ValueError('The scale factor for the exposure time must be > 0 '
                         ' and <= 1.')

    median_dark = np.median(dark, axis=0)

    white_counts = white - median_dark[None,:]
    scaled_white = np.round(np.random.poisson(white_counts * factor) +
        median_dark[None,:]).astype(int)

    prj_counts = prj - median_dark[None, :]
    scaled_prj = np.round(np.random.poisson(prj_counts * factor) +
        median_dark[None,:]).astype(int)

    return scaled_prj, dark.copy(), scaled_white

def reduce_projections(factor, prj, theta):
    """
    Simulate a reduction of the number of exposures in the data set.

    Parameters
    ----------
    factor : int
        The factor by which the number of exposures should be reduced.
    prj : array-like
        The original projection data to be reduced.
    theta : 1D array-like
        The original theta array to be reduced.

    Returns
    -------
    reduced_prj : array-like
        The projection data with reduced number of projections
    reduced_theta : 1D array-like
        The theta values with the reduced number of positions

    """

    if not factor == int(factor):
        print('WARNING: The projection reduction factor must be a positive '
              'integer.')

    factor = int(factor)
    reduced_prj = prj[::factor, :]
    reduced_theta = theta[::factor]

    return reduced_prj, reduced_theta

def load_scan_series(dataset, scan_time=1):
    """
    Simulate a time-series measurement with different exposure times.

    This loads a time-series data sets with a total of 75 scans and an
    individual scan time of 1 ms and simulates longer scan times by averaging
    over multiple scans.

    Parameters
    ----------
    dataset : str {'coffee', 'metal_foam', 'foam'}
        The name of the dataset to load.
    scan_time : int
        The simulated scan time in number of real scans that should be merged.
        Must be an integer >0. (default = 1)

    Returns
    -------
    scan_data : list
        The list of projection data sets for all simulated time steps.
    data_dark: 2D array
        The dark field data set.
    data_white : 2D array
        The white field data set.
    scan_theta : list
        The list of theta-arrays for the individual scans in the time-series.

    """

    scan_data = []
    scan_theta = []

    if dataset in ('coffee', 'foam'):
        if dataset == 'coffee':
            datafile='Data/GF_nespresso_40prj_goldenratio_01_slice550_cropped500.h5'
        elif dataset == 'foam':
            datafile='Data/GF_foam_dry_40prj_goldenratio_02_slice895.h5'
        spt = 0.05  # seconds per tomography scan
        prj, dark, white, theta = load_data(datafile)
        scan_id = (np.floor(theta / (180.0 * scan_time))).astype(int)
        scan_ids = (np.unique(scan_id)).astype(int)
        for id in scan_ids:
            scan_data.append(prj[scan_id == id, :])
            scan_theta.append(theta[scan_id == id])
        step = np.round(len(scan_id) / len(scan_ids)).astype(int)
    elif dataset == 'metal_foam':
        datafile='Data/149_ASM_SP_1ktps_subset_withTheta_slice50.h5'
        spt = 0.001 # seconds per tomography scan
        prj, dark, white, theta = load_data(datafile)
        # data set has 40 projections / 180 degrees
        step = scan_time * 40
        for p1 in range(0, prj.shape[0], step):
            scan_data.append(prj[p1:p1+step,:])
            scan_theta.append(theta[p1:p1+step])

    print("Ran {} scans with a scan time of {} s each.".format(
        len(scan_data), scan_time * spt))
    print("Number of projections per scan: {}".format(step))

    return scan_data, dark, white, scan_theta
