{
 "cells": [
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "# Part 1: A guided walk through the first data set\n",
    "\n",
    "## The first reconstruction"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "scrolled": true
   },
   "outputs": [],
   "source": [
    "# Load the necessary packages and prepare the python environment\n",
    "import matplotlib.pyplot as plt\n",
    "import numpy as np\n",
    "import time\n",
    "from skimage.filters import threshold_otsu\n",
    "\n",
    "from tomography_tutorial_functions import *\n",
    "from paganin_functions import *\n",
    "\n",
    "%matplotlib inline"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "scrolled": true
   },
   "outputs": [],
   "source": [
    "# define some parameters for the later processing\n",
    "binning = 1\n",
    "padding = 0.5\n",
    "max_width = 500\n",
    "\n",
    "# load the raw data from file\n",
    "prj, dark, white, theta = load_data('Data/ZF_21keV_650nm_40mm_slice500.h5', max_width=max_width, binning=binning)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "The raw data needs to be corrected for the flat field illumination and the detector dark counts to produce a series of high-quality projection images (1-D in our case):"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "cpr = correct_data(prj, dark, white)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "The flat and dark field-corrected projections give a spatially resolved measurement of the amount of transmitted intensity through the sample:\n",
    "\n",
    "$ \\mathrm{cpr}(x,y) = \\frac{I}{I_0}$.\n",
    "\n",
    "What we are actually interested in is a measure of the X-ray absorption coefficient $\\mu$. According to the Beer-Lambert law (see presentation), we have:\n",
    "\n",
    "$ \\frac{I}{I_0} = e^{-\\mu d}$,\n",
    "\n",
    "where $d$ is the thickness of the sample, and $\\mu$ the (average) absorption coefficient of the material. For a spatially inhomogeneous sample, the last term is replaced by an integration over the local absorption coefficient along the X-ray beam path.\n",
    "\n",
    "The exact deriavation of the reconstruction process is presented in the presentation or in general text books (see for example AC: Kak, M Slaney, “Principles of Computerized Tomographic Imaging”, IEEE Press 1988. http://www.slaney.org/pct/pct-toc.html). For our tutorial, it is important to realize that we are interested in reconstructing the spatially resolved map of absorption coefficients in the sample. So what we need as an input for the reconstruction is something that is proportional to $\\mu$:\n",
    "\n",
    "$ \\mu = -ln(\\frac{I}{I_0}) \\frac{1}{d} $.\n",
    "\n",
    "This is why we calculate the so-called sinogram from the negative logarithm of the cpr array:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# calculate the sinogram\n",
    "sino = -np.log(cpr)\n",
    "# show the sinogram image\n",
    "plt.figure(figsize=[12,6])\n",
    "plt.imshow(sino.T, cmap='gray')\n",
    "plt.xlabel('projection number (angle)')\n",
    "plt.ylabel('(binned) pixel coordinate')"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Now we can run the reconstruction on this sinogram image (using the inverse Radon transform, see https://scikit-image.org/docs/dev/auto_examples/transform/plot_radon_transform.html), and providing the measured theta angle for each of the projections. Note that the reconstruction process itself will take a while (the larger the input image, the longer the calculation...)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "reco1 = reconstruct(sino, theta, padding=padding)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# Plot the reconstructed image\n",
    "plt.figure(figsize=[9,9])\n",
    "plt.imshow(reco1, cmap='gray')"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Fixing the rotation center\n",
    "As you can see, the above imaged does not look very clean, there are some strange artefacts in the reconstruction. In this case, they are caused by the fact that the rotation center of the tomographic rotation stage was not perfectly centered on the projection. Such an offset in the rotation center causes C-shaped artefacts, and their orientation depends on the direction in which the actual center is offset from the center of the detector. For the reconstructed image orientation used in this tutorial (as given by the `iradon` function in `skimage.filters`), the C-shapes are oriented in the following manner:\n",
    "\n",
    "* $\\cap \\Rightarrow$ the rotation center is too low, it needs to be increased\n",
    "* $\\cup \\Rightarrow$ the rotation center is too high, it needs to be lowered\n",
    "\n",
    "### Question:\n",
    "How do we need to adjust the rotation center in the above reconstruction? Make it lower or higher?\n",
    "\n",
    "### Exercise:\n",
    "Try to adjust the rotation center for the reconstruction until you get a good result. We will need this center value for the rest of Part 1 of this tutorial."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# Adjust the center value (replace \"None\" with an actual number) until you get a good reconstruction.\n",
    "# Look at the output of the above reconstruction function to get a good starting guess.\n",
    "center = None\n",
    "padding = 0.45\n",
    "reco_c = reconstruct(-np.log(cpr), theta, center=center, padding=padding)\n",
    "plt.figure(figsize=[9,9])\n",
    "plt.imshow(reco_c, cmap='gray')"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Bonus question:\n",
    "Can you guess what we are looking at in this reconstruction?"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": []
  }
 ],
 "metadata": {
  "kernelspec": {
   "display_name": "Python 3",
   "language": "python",
   "name": "python3"
  },
  "language_info": {
   "codemirror_mode": {
    "name": "ipython",
    "version": 3
   },
   "file_extension": ".py",
   "mimetype": "text/x-python",
   "name": "python",
   "nbconvert_exporter": "python",
   "pygments_lexer": "ipython3",
   "version": "3.8.5"
  }
 },
 "nbformat": 4,
 "nbformat_minor": 4
}
