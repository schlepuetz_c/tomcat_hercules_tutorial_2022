{
 "cells": [
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "# Part 2: Effect of acquisition parameters on image quality\n",
    "\n",
    "Let's look at another sample to get a bit of a feel for how different acquisition parameters can affect the image quality.\n",
    "\n",
    "The sample is a thin glas capilary with 1.5 mm diameter which is filled with small PPMA spheres of nominally around 90 - 110 microns in diameter. We are looking at one central slice through the sample. The data set has been acquired under four different conditions: two measurements at a larger distance between the sample and detector (305 mm), and two with a short distance (30 mm). For each pair, one measurement was performed with the longest possible exposure time without saturating the detector (2.5 ms -> highSNR), and one with a much shorter exposure time to simulate a situation where we are limited by the amount of light that we can get (0.2 ms -> lowSNR). All scans have been performed with a very large number of projections (8000).\n",
    "\n",
    "First, let us have a look at the full high-quality data set (caution, this will take a while to compute!). The parameters below have already been adjusted with the correct rotation center."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "scrolled": true
   },
   "outputs": [],
   "source": [
    "# Load the necessary packages and prepare the python environment\n",
    "import matplotlib.pyplot as plt\n",
    "import numpy as np\n",
    "import time\n",
    "from skimage.filters import threshold_otsu\n",
    "\n",
    "from tomography_tutorial_functions import *\n",
    "from paganin_functions import *\n",
    "\n",
    "%matplotlib inline"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# The full width of the interesting part of the image is 620 pixels.\n",
    "# However, this uses too much memory if running things on binder.org,\n",
    "# so we need cut the dataset down to 500 pixels in this case.\n",
    "# If you are running the script locally, you can look at the full 620\n",
    "# pixels by changing the value below.\n",
    "#max_width = 620\n",
    "max_width = 500\n",
    "\n",
    "dataset = 'Data/GF_spheres_dry_8000prj_305mm_highSNR_01_slice310.h5'\n",
    "prj, dark, white, theta = load_data(dataset, max_width=max_width)\n",
    "\n",
    "image_width = prj.shape[1]\n",
    "center = (image_width / 2.0) - 2\n",
    "padding = 0.45\n",
    "\n",
    "cpr = correct_data(prj, dark, white)\n",
    "sino = -np.log(cpr)\n",
    "reco = reconstruct(sino, theta, center=center, padding=padding)\n",
    "plt.figure(figsize=[9,9])\n",
    "plt.imshow(reco, cmap='gray')"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Reduced number of projections\n",
    "One way to make a tomography scan faster is to reduce the overall number of projections that are acquired over 180 degrees. Of course, this has consequences for the image quality and the SNR.\n",
    "\n",
    "Starting from the above data set, lets decrease the number of projections more and more to see what happens."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# Produce reconstruction by decreasing the number of projections by 5x, 10x, 20x, 50x, 100x\n",
    "prj_n5, theta_n5 = reduce_projections(5, prj, theta)\n",
    "prj_n10, theta_n10 = reduce_projections(10, prj, theta)\n",
    "prj_n20, theta_n20 = reduce_projections(20, prj, theta)\n",
    "prj_n50, theta_n50 = reduce_projections(50, prj, theta)\n",
    "prj_n100, theta_n100 = reduce_projections(100, prj, theta)\n",
    "prj_n250, theta_n250 = reduce_projections(250, prj, theta)\n",
    "\n",
    "cpr_n5 = correct_data(prj_n5, dark, white)\n",
    "reco_n5 = reconstruct(-np.log(cpr_n5), theta_n5, center=center, padding=padding)\n",
    "cpr_n10 = correct_data(prj_n10, dark, white)\n",
    "reco_n10 = reconstruct(-np.log(cpr_n10), theta_n10, center=center, padding=padding)\n",
    "cpr_n20 = correct_data(prj_n20, dark, white)\n",
    "reco_n20 = reconstruct(-np.log(cpr_n20), theta_n20, center=center, padding=padding)\n",
    "cpr_n50 = correct_data(prj_n50, dark, white)\n",
    "reco_n50 = reconstruct(-np.log(cpr_n50), theta_n50, center=center, padding=padding)\n",
    "cpr_n100 = correct_data(prj_n100, dark, white)\n",
    "reco_n100 = reconstruct(-np.log(cpr_n100), theta_n100, center=center, padding=padding)\n",
    "cpr_n250 = correct_data(prj_n250, dark, white)\n",
    "reco_n250 = reconstruct(-np.log(cpr_n250), theta_n250, center=center, padding=padding)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "plt.figure(figsize=[16,24])\n",
    "plt.subplot(3,2,1)\n",
    "plt.imshow(reco, cmap='gray')\n",
    "plt.title(\"original: {} projections\".format(len(theta)))\n",
    "#plt.subplot(3,2,1)\n",
    "#plt.imshow(reco_n5, cmap='gray')\n",
    "#plt.title(\"#projections reduced by 5x: {}\".format(len(theta_n5)))\n",
    "plt.subplot(3,2,2)\n",
    "plt.imshow(reco_n10, cmap='gray')\n",
    "plt.title(\"#projections reduced by 10x: {}\".format(len(theta_n10)))\n",
    "plt.subplot(3,2,3)\n",
    "plt.imshow(reco_n20, cmap='gray')\n",
    "plt.title(\"#projections reduced by 20x: {}\".format(len(theta_n20)))\n",
    "plt.subplot(3,2,4)\n",
    "plt.imshow(reco_n50, cmap='gray')\n",
    "plt.title(\"#projections reduced by 50x: {}\".format(len(theta_n50)))\n",
    "plt.subplot(3,2,5)\n",
    "plt.imshow(reco_n100, cmap='gray')\n",
    "plt.title(\"#projections reduced by 100x: {}\".format(len(theta_n100)))\n",
    "plt.subplot(3,2,6)\n",
    "plt.imshow(reco_n250, cmap='gray')\n",
    "plt.title(\"#projections reduced by 250x: {}\".format(len(theta_n250)))\n"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Reduced exposure time\n",
    "Another method to reduce scan times is to decrease the exposure time for each projection. Also this will affect the signal to noise (SNR) in the reconstructed image. Below you can see an example of reducing the exposure times by a factor of 10 or 100 for the above reconstruction. And we do the same thing for the lowSNR data set as well."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# Load the high and low SNR datasets\n",
    "\n",
    "dataset = 'Data/GF_spheres_dry_8000prj_305mm_highSNR_01_slice310.h5'\n",
    "prj, dark, white, theta = load_data(dataset, max_width=max_width)\n",
    "dataset2 = 'Data/GF_spheres_dry_8000prj_305mm_lowSNR_01_slice310.h5'\n",
    "prj2, dark2, white2, theta2 = load_data(dataset2, max_width=max_width)\n",
    "\n",
    "# Reduce the number of projections 10x to make things a bit faster (no significant loss in quality, as seen above)\n",
    "prj, theta = reduce_projections(10, prj, theta)\n",
    "prj2, theta2 = reduce_projections(10, prj2, theta2)\n",
    "\n",
    "image_width = prj.shape[1]\n",
    "center = (image_width / 2.0) - 2\n",
    "padding = 0.45\n",
    "\n",
    "cpr = correct_data(prj, dark, white)\n",
    "sino = -np.log(cpr)\n",
    "reco = reconstruct(sino, theta, center=center, padding=padding)\n",
    "\n",
    "cpr2 = correct_data(prj2, dark2, white2)\n",
    "sino2 = -np.log(cpr2)\n",
    "reco2 = reconstruct(sino2, theta2, center=center, padding=padding)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# Simulate data sets with a reduced exposure time\n",
    "prj_e10, dark_e10, white_e10 = reduce_exposure_time(0.1, prj, dark, white)\n",
    "prj_e100, dark_e100, white_e100 = reduce_exposure_time(0.01, prj, dark, white)\n",
    "\n",
    "prj2_e10, dark2_e10, white2_e10 = reduce_exposure_time(0.1, prj2, dark2, white2)\n",
    "prj2_e100, dark2_e100, white2_e100 = reduce_exposure_time(0.01, prj2, dark2, white2)\n",
    "\n",
    "# Calculate the reconstructions for the data sets with lower exposure times\n",
    "#   Setting verbose=False suppressed the output of the reconstruction command\n",
    "cpr_e10 = correct_data(prj_e10, dark_e10, white_e10)\n",
    "reco_e10 = reconstruct(-np.log(cpr_e10), theta, center=center, padding=padding, verbose=False)\n",
    "cpr_e100 = correct_data(prj_e100, dark_e100, white_e100)\n",
    "reco_e100 = reconstruct(-np.log(cpr_e100), theta, center=center, padding=padding, verbose=False)\n",
    "cpr2_e10 = correct_data(prj2_e10, dark2_e10, white2_e10)\n",
    "reco2_e10 = reconstruct(-np.log(cpr2_e10), theta2, center=center, padding=padding, verbose=False)\n",
    "cpr2_e100 = correct_data(prj2_e100, dark2_e100, white2_e100)\n",
    "reco2_e100 = reconstruct(-np.log(cpr2_e100), theta2, center=center, padding=padding, verbose=False)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# Compare the reconstructions for the different exposure times\n",
    "plt.figure(figsize=[16,24])\n",
    "plt.subplot(3,2,1)\n",
    "plt.imshow(reco, cmap='gray')\n",
    "plt.title(\"Original highSNR\")\n",
    "plt.subplot(3,2,2)\n",
    "plt.imshow(reco2, cmap='gray')\n",
    "plt.title(\"Original lowSNR\")\n",
    "plt.subplot(3,2,3)\n",
    "plt.imshow(reco_e10, cmap='gray')\n",
    "plt.title(\"high SNR exposure reduced 10x\")\n",
    "plt.subplot(3,2,4)\n",
    "plt.imshow(reco2_e10, cmap='gray')\n",
    "plt.title(\"low SNR exposure reduced 10x\")\n",
    "plt.subplot(3,2,5)\n",
    "plt.imshow(reco_e100, cmap='gray')\n",
    "plt.title(\"high SNR exposure reduced 100x\")\n",
    "plt.subplot(3,2,6)\n",
    "plt.imshow(reco2_e100, cmap='gray')\n",
    "plt.title(\"low SNR exposure reduced 100x\")"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "# Propagation distance\n",
    "\n",
    "The distance between the sample and the detector also affects the quality of the image substantially. This is particularly true at a synchrotron, where the refraction effects of the partially coherent beam at the interfaces between different materials in the sample produce a distinct enhancement of these boundaries, an effect which is called *edge enhancement*.\n",
    "\n",
    "Comparing the reconstruction from above with a larger propagation distance of 305 mm with one that was taken with the detector at only 30 mm from the sample reveals the edge enhancement clearly.\n"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# Reconstruct the high SNR dataset taken at the short propagation distance\n",
    "dataset3 = 'Data/GF_spheres_dry_8000prj_30mm_highSNR_01_slice310.h5'\n",
    "prj3, dark3, white3, theta3 = load_data(dataset3, max_width=max_width)\n",
    "\n",
    "# Reduce the number of projections 10x\n",
    "prj3, theta3 = reduce_projections(10, prj3, theta3)\n",
    "\n",
    "image_width = prj.shape[1]\n",
    "center = (image_width / 2.0) - 4\n",
    "padding = 0.45\n",
    "\n",
    "cpr3 = correct_data(prj3, dark3, white3)\n",
    "sino3 = -np.log(cpr3)\n",
    "reco3 = reconstruct(sino3, theta3, center=center, padding=padding, verbose=False)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# Compare the reconstructions for the two propagation distances\n",
    "plt.figure(figsize=[16,16])\n",
    "plt.subplot(1,2,1)\n",
    "plt.imshow(reco, cmap='gray')\n",
    "plt.title(\"Large propagation distance\")\n",
    "plt.subplot(1,2,2)\n",
    "plt.imshow(reco3, cmap='gray')\n",
    "plt.title(\"Small propagation distance\")"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": []
  }
 ],
 "metadata": {
  "kernelspec": {
   "display_name": "Python 3",
   "language": "python",
   "name": "python3"
  },
  "language_info": {
   "codemirror_mode": {
    "name": "ipython",
    "version": 3
   },
   "file_extension": ".py",
   "mimetype": "text/x-python",
   "name": "python",
   "nbconvert_exporter": "python",
   "pygments_lexer": "ipython3",
   "version": "3.8.5"
  }
 },
 "nbformat": 4,
 "nbformat_minor": 4
}
