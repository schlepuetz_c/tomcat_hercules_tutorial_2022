{
 "cells": [
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "# Part 5: The dose optimization challenge\n",
    "\n",
    "The guided walk in part 1 should have given you a basic impression of the different steps involved in reconstructing a tomographic data set and some of the quirks (rotation center) and experiment design choices (number of projections, exposure time, contrast mechanism) one encounters along the way.\n",
    "\n",
    "This second part of the tutorial is meant to be much more interactive. Now it's play time! So here is the challenge.\n",
    "\n",
    "## Dose vs. image quality\n",
    "\n",
    "One important consideration when using X-rays for medical or diagnostic purposes or in conjunction with radiation-sensitive samples is the total radiation dose imparted on the sample by a full CT scan. The dose is, of course, directly proportional to the total X-ray exposure time (number of projections $N$ times the exposure time per projection $t_p$). Usually, one wants to keep this dose as small as possible for any given scan. But as we have seen above, eventually the image quality will be compromised. So the fundamental challenge in X-ray imaging is always to get the best possible image quality for the lowest possible dose.\n",
    "\n",
    "## Exercise:\n",
    "\n",
    "In this exercise, you will perform the whole analysis pipeline from part 1 on a different sample. Here is what you should do:\n",
    "\n",
    "* From the below list of sample data sets, pick one of the files starting with \"uCT\" for the exercise (the \"ZF\" one is  the zebrafish we used above).\n",
    "* Reconstruct the data set to get a sense for the type of sample you have.\n",
    "* Now try to reduce the total X-ray exposure as much as possible while keeping a useable image quality, making use of the following tools from above:\n",
    "  * reduce the exposure time per projection (`reduce_exposure_time()`)\n",
    "  * reduce the number of projections (`reduce_projections()`)\n",
    "  * find a suitable set of phase reconstruction parameters, if desired (see note below)\n",
    "* Try a basic thresholding operation on the reconstruction result to check whether you are still able to get quantitative information from the sample.\n",
    "* Once you have found the best result for the smallest possible dose, calculate the dose reduction factor you have achieved with respect to the original data set.\n",
    "\n",
    "## Question:\n",
    "What is the dose reduction factor you were able to achieve?\n",
    "  \n",
    "  \n",
    "### Note for phase reconstructions\n",
    "The experimental parameters are fixed and the corresponding values are given in the below table for each samples. The only free parameters to influence the contrast in the reconstruction are the *beta* and *delta* parameters. While both of those are in principle adjustable, it is mostly the ratio of the two that has the biggest impact on the image contrast. Therefore, it is easiest if you fix one of the two (at around the values used in the example in Part 1), and you simply adjust the other one. This way, the whole optimization boils down to tweaking this one parameter.  "
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## List of data sets\n",
    "\n",
    "Here are a few other datasets to test with the relevant acquisition parameters:\n",
    "\n",
    "| Dataset | Energy [keV] | Pixel size [um] | Distance [m] | # of pixels | # of projections | Description |\n",
    "| :--- | :---: | :---: | :---: | :---: | :---: | :--- |\n",
    "| GF_spheres_dry_8000prj_305mm_highSNR_01_slice310.h5 | peak @ 20 | 2.75 | 0.305 | 720 | 8000 | PMMA spheres |\n",
    "| GF_spheres_dry_8000prj_305mm_lowSNR_01_slice310.h5 | peak @ 20 | 2.75 | 0.305 | 720 | 8000 | PMMA spheres |\n",
    "| GF_spheres_dry_8000prj_30mm_highSNR_01_slice310.h5 | peak @ 20 | 2.75 | 0.030 | 720 | 8000 | PMMA spheres |\n",
    "| GF_spheres_dry_8000prj_30mm_lowSNR_01_slice310.h5 | peak @ 20 | 2.75 | 0.030 | 720 | 8000 | PMMA spheres |\n",
    "| ZF_21keV_650nm_40mm_slice500.h5 | 21.0 | 0.65 | 0.04 | 980 | 2001 | Zebrafish embryo head |\n",
    "| ZF_eye_68mm_650nm_21keV_01_slice1230.h5 | 21.0 | 0.65 | 0.068 | 2560 | 2000 | Zebrafish embryo eye|\n",
    "| ZF_eye_68mm_650nm_21keV_01_slice650.h5 | 21.0 | 0.65 | 0.068 | 2560 | 2000 | Zebrafish embryo otholith |\n",
    "| ZF_eye_8mm_650nm_21keV_02_slice1230.h5 | 21.0 | 0.65 | 0.008 | 2560 | 2000 | Zebrafish embryo eye|\n",
    "| ZF_eye_8mm_650nm_21keV_02_slice675.h5 | 21.0 | 0.65 | 0.008 | 2560 | 2000 | Zebrafish embryo otholith |\n",
    "| ZF_tail_68mm_650nm_21keV_01_slice1630.h5 | 21.0 | 0.65 | 0.068 | 2560 | 2000 | Zebrafish embryo tail section |\n",
    "| uCT_ref_002_bamboo_01_slice650.h5 | 12.0 | 1.625 | 0.01 | 1850 | 1801 | Bamboo cocktail stick |\n",
    "| uCT_ref_007_match_02_slice800.h5 | 15.0 | 1.625 | 0.01 | 2270 | 1801 | match stick with head |\n",
    "| uCT_ref_009_white_patch_01_slice1140.h5 | 12.0 | 1.625 | 0.01 | 1280 | 1801 | Felt glider patch | "
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "scrolled": true
   },
   "outputs": [],
   "source": [
    "# Load the necessary packages and prepare the python environment\n",
    "import matplotlib.pyplot as plt\n",
    "import numpy as np\n",
    "import time\n",
    "from skimage.filters import threshold_otsu\n",
    "\n",
    "from tomography_tutorial_functions import *\n",
    "from paganin_functions import *\n",
    "\n",
    "%matplotlib inline"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "binning = 1\n",
    "max_width = 700\n",
    "\n",
    "dataset = 'Data/uCT_ref_002_bamboo_01_slice650.h5'\n",
    "#dataset = 'Data/uCT_ref_007_match_02_slice800.h5'\n",
    "#dataset = 'Data/uCT_ref_009_white_patch_01_slice1140.h5'\n",
    "#dataset = 'Data/ZF_21keV_650nm_40mm_slice500.h5'\n",
    "#dataset = 'Data/GF_spheres_dry_8000prj_305mm_highSNR_01_slice310.h5'\n",
    "#dataset = 'Data/GF_spheres_dry_8000prj_305mm_lowSNR_01_slice310.h5'\n",
    "#dataset = 'Data/GF_spheres_dry_8000prj_30mm_highSNR_01_slice310.h5'\n",
    "#dataset = 'Data/GF_spheres_dry_8000prj_30mm_lowSNR_01_slice310.h5'\n",
    "#dataset = 'Data/ZF_eye_68mm_650nm_21keV_01_slice1230.h5'\n",
    "#dataset = 'Data/ZF_eye_68mm_650nm_21keV_01_slice650.h5'\n",
    "#dataset = 'Data/ZF_eye_8mm_650nm_21keV_02_slice1230.h5'\n",
    "#dataset = 'Data/ZF_eye_8mm_650nm_21keV_02_slice675.h5'\n",
    "#dataset = 'Data/ZF_tail_68mm_650nm_21keV_01_slice1630.h5'\n",
    "\n",
    "prj, dark, white, theta = load_data(dataset, max_width=max_width, binning=binning)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "cen = (prj.shape[1] / 2.0) - 33\n",
    "padding = 0.45\n",
    "\n",
    "prj1, theta1 = reduce_projections(2, prj, theta)\n",
    "\n",
    "cpr = correct_data(prj1, dark, white)\n",
    "sino = -np.log(cpr)\n",
    "reco = reconstruct(sino, theta1, center=cen, padding=padding)\n",
    "plt.figure(figsize=[9,9])\n",
    "plt.imshow(reco, cmap='gray')"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# Your code here...\n",
    "# (Copy-paste functions from the different tutorial parts as much as you need.)\n",
    "# (Insert more cells as needed with the \"+\" button in the toolbar)\n",
    "\n",
    "\n",
    "\n",
    "\n",
    "\n",
    "\n",
    "\n"
   ]
  }
 ],
 "metadata": {
  "kernelspec": {
   "display_name": "Python 3",
   "language": "python",
   "name": "python3"
  },
  "language_info": {
   "codemirror_mode": {
    "name": "ipython",
    "version": 3
   },
   "file_extension": ".py",
   "mimetype": "text/x-python",
   "name": "python",
   "nbconvert_exporter": "python",
   "pygments_lexer": "ipython3",
   "version": "3.8.5"
  }
 },
 "nbformat": 4,
 "nbformat_minor": 4
}
